# Aplicación de prueba android

Aplicación con arquitectura MVVM que se conecta al API de themoviedb.com, baja las peliculas y series categorizadas por Now playing y Popular, usa Retrofit2 y corutinas para hacer la conexión al API y el resultado lo presenta por separado en un Recyclerview dentro de un ViewPager2, al hacer clic en un elemento muestra el detalle de la Pelicula/serie con el trailer en video incrustado (si está disponible) usando la libreria de YouTubePlayer para android.

## Pendientes
■ Ya está configurada para implementar Inyección de dependencias con Dagger hilt.

■ Implementar un sistema de caché para uso offline, está preparada con un repositorio para poder hacer la implementación ya sea directamente con Retrofit, alguna libreria externa como coroutinecache o incluso con base de datos. 
