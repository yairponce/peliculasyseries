package com.dyd.peliculasyseries

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PeliculasSeriesApp:Application()