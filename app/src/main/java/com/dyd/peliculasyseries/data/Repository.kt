package com.dyd.peliculasyseries.data

import MovieModel
import com.dyd.peliculasyseries.data.network.RetrofitService
import dagger.hilt.android.qualifiers.ActivityContext
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Response
import java.lang.Exception
import java.net.InetAddress

class Repository constructor(private val retrofitService: RetrofitService) {

    suspend fun getNowPlayingMovies() = retrofitService.getNowPlayingMovies()

    suspend fun getMostPopularMovies() = retrofitService.getMostPopularMovies()

    suspend fun getNowPlayingTV() =  retrofitService.getNowPlayingTV()

    suspend fun getMostPopularTV() = retrofitService.getMostPopularTV()

    suspend fun getTVVideo(id:Int) =  retrofitService.getTVVideo(id)

    suspend fun getMovieVideo(id:Int) = retrofitService.getMovieVideo(id)


}