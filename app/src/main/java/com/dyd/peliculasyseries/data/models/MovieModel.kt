data class MovieModel(
    val page: Int,
    val results: List<MovieOverviewModel>,
    val total_pages: Int,
    val total_results: Int
)