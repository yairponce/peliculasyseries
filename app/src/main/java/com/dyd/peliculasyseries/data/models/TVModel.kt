package com.dyd.peliculasyseries.data.models

data class TVModel(
    val page: Int,
    val results: List<TVOverviewModel>,
    val total_pages: Int,
    val total_results: Int
)