package com.dyd.peliculasyseries.data.models

data class VideosModel(
    val id: Int,
    val results: List<VideosResultModel>
)