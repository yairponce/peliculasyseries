package com.dyd.peliculasyseries.data.network

import MovieModel
import com.dyd.peliculasyseries.data.models.TVModel
import com.dyd.peliculasyseries.data.models.VideosModel
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.lang.Exception
import java.net.InetAddress

interface RetrofitService {

    @GET("/3/movie/now_playing/")
    suspend fun getNowPlayingMovies(
        @Query("api_key") apiKey: String = ApiConstants.API_KEY,
        @Query("language") lang: String = ApiConstants.LANGUAGE
    ):Response<MovieModel>

    @GET("/3/movie/popular/")
    suspend fun  getMostPopularMovies(
        @Query("api_key") apiKey: String = ApiConstants.API_KEY,
        @Query("language") lang: String = ApiConstants.LANGUAGE
    ):Response<MovieModel>

    @GET("/3/tv/airing_today/")
    suspend fun getNowPlayingTV(
        @Query("api_key") apiKey: String = ApiConstants.API_KEY,
        @Query("language") lang: String = ApiConstants.LANGUAGE
    ):Response<TVModel>

    @GET("/3/tv/popular/")
    suspend fun  getMostPopularTV(
        @Query("api_key") apiKey: String = ApiConstants.API_KEY,
        @Query("language") lang: String = ApiConstants.LANGUAGE
    ):Response<TVModel>

    @GET("/3/movie/{id}/videos")
    suspend fun getMovieVideo(
        @Path("id") id:Int,
        @Query("api_key") apiKey: String = ApiConstants.API_KEY,
        @Query("language") lang: String = ApiConstants.LANGUAGE
    ):Response<VideosModel>

    @GET("/3/tv/{id}/videos")
    suspend fun getTVVideo(
        @Path("id") id:Int,
        @Query("api_key") apiKey: String = ApiConstants.API_KEY,
        @Query("language") lang: String = ApiConstants.LANGUAGE
    ):Response<VideosModel>

    companion object {
        var retrofitService: RetrofitService? = null

        fun getInstance() : RetrofitService {
            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl(ApiConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }
}