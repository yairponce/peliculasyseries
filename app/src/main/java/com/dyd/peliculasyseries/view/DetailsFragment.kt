package com.dyd.peliculasyseries.view

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import android.widget.TextView
import android.widget.Toast
import android.widget.VideoView
import androidx.fragment.app.*
import androidx.lifecycle.Observer
import com.dyd.peliculasyseries.R
import com.dyd.peliculasyseries.data.network.ApiConstants
import com.dyd.peliculasyseries.viewmodel.MainActivityViewModel
import com.google.android.youtube.player.*


class DetailsFragment : DialogFragment() {

    private val detailViewModel: MainActivityViewModel by viewModels()

    override fun onCreateDialog(
        savedInstanceState: Bundle?
    ): Dialog {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("--")
        builder.setView(R.layout.fragment_details)
        return builder.create()
    }

    override fun onStart() {
        super.onStart()
        val args = arguments
        dialog!!.setTitle(args!!.getString("title"))

        val overview = dialog!!.findViewById<TextView>(R.id.tvOverviewDialog)
        val date = dialog!!.findViewById<TextView>(R.id.tvDate)

        //YouTube implementation
        val youTubePlayerFragment = YouTubePlayerSupportFragmentX.newInstance()
        val manager: FragmentManager? = childFragmentManager
        val transaction: FragmentTransaction? = manager?.beginTransaction()
        transaction?.replace(R.id.youtubeFragment, youTubePlayerFragment)?.commit()
        //Youtube

        overview.text = args.getString("overview")
        date.text = args.getString("date")

        detailViewModel.videoMovie.observe(this, Observer {
            if (!it.results.isNullOrEmpty()) {
                youTubePlayerFragment.initialize(ApiConstants.YT_API_KEY,
                    object : YouTubePlayer.OnInitializedListener {
                        override fun onInitializationSuccess(
                            provider: YouTubePlayer.Provider,
                            youTubePlayer: YouTubePlayer, b: Boolean
                        ) {
                            youTubePlayer.cueVideo(it.results.get(0).key)
                        }

                        override fun onInitializationFailure(
                            provider: YouTubePlayer.Provider,
                            youTubeInitializationResult: YouTubeInitializationResult
                        ) {
                        }
                    })
            }
        })

        detailViewModel.videoTV.observe(this, Observer {
            if (!it.results.isNullOrEmpty()) {
                youTubePlayerFragment.initialize(ApiConstants.YT_API_KEY,
                    object : YouTubePlayer.OnInitializedListener {
                        override fun onInitializationSuccess(
                            provider: YouTubePlayer.Provider,
                            youTubePlayer: YouTubePlayer, b: Boolean
                        ) {
                            youTubePlayer.cueVideo(it.results.get(0).key)
                        }

                        override fun onInitializationFailure(
                            provider: YouTubePlayer.Provider,
                            youTubeInitializationResult: YouTubeInitializationResult
                        ) {
                        }
                    })
            }
        })

        if (args.getBoolean("movie")) {
            detailViewModel.getVideoMovie(args.getInt("id"))
        } else {
            detailViewModel.getVideoTV(args.getInt("id"))
        }

    }
}