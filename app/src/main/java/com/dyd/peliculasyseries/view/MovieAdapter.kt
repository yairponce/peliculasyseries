package com.dyd.peliculasyseries.view

import MovieModel
import MovieOverviewModel
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dyd.peliculasyseries.R
import com.dyd.peliculasyseries.data.network.ApiConstants

class MovieAdapter(private val movies: MovieModel,private val onClickListener: OnClickListener) : RecyclerView.Adapter<MovieAdapter.viewHolder>() {

    class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var movieImage: ImageView = itemView.findViewById(R.id.ivPoster)
        var movieTitle: TextView = itemView.findViewById(R.id.tvTitle)
        var movieExtra: TextView = itemView.findViewById(R.id.tvExtra)
        var movieOverview: TextView = itemView.findViewById(R.id.tvOverview)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row, parent, false)
        return viewHolder(itemView)
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        var title = movies.results.get(position).title + " (" +movies.results.get(position).vote_average +")"
        holder.movieTitle.text = title
        holder.movieOverview.text = movies.results.get(position).overview
        holder.movieExtra.text = movies.results.get(position).release_date
        Glide.with(holder.itemView)
            .load(ApiConstants.IMG_BASE_URL+movies.results.get(position).poster_path)
            .into(holder.movieImage)

        val item = movies.results.get(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(item)
        }
    }

    class OnClickListener(val clickListener: (item:MovieOverviewModel) -> Unit) {
        fun onClick(item:MovieOverviewModel) = clickListener(item)
    }

    override fun getItemCount(): Int {
        return movies.results.size
    }
}