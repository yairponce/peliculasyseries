package com.dyd.peliculasyseries.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.dyd.peliculasyseries.R
import com.dyd.peliculasyseries.viewmodel.MainActivityViewModel
import androidx.recyclerview.widget.LinearLayoutManager

import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.material.switchmaterial.SwitchMaterial


class PlayingNowFragment : Fragment() {
    private lateinit var progressBar: ProgressBar
    private lateinit var rvPlayingNow: RecyclerView
    private lateinit var switch: SwitchMaterial

    private val moviesViewModel: MainActivityViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_playing_now, container, false)
        progressBar = root.findViewById(R.id.pbPlayingNow)
        rvPlayingNow = root.findViewById(R.id.rvPlayingNow)
        switch = root.findViewById(R.id.swTypePlayingNow)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        moviesViewModel.moviesNowPlaying.observe(this, Observer {
            val adapter = MovieAdapter(it,MovieAdapter.OnClickListener{
                val args = Bundle()
                args.putInt("id", it.id)
                args.putString("title", it.title)
                args.putString("overview", it.overview)
                args.putString("date",it.release_date)
                args.putBoolean("movie",true)
                val dialog = DetailsFragment()
                dialog.arguments = args
                dialog.show(parentFragmentManager,"")
            })
            val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
            rvPlayingNow.addItemDecoration(
                DividerItemDecoration(
                    activity,
                    LinearLayoutManager.VERTICAL
                )
            )
            rvPlayingNow.adapter=adapter
            rvPlayingNow.layoutManager=layoutManager
        })

        moviesViewModel.TVNowPlaying.observe(this, Observer {
            val adapter = TVShowAdapter(it,TVShowAdapter.OnClickListener{
                val args = Bundle()
                args.putInt("id", it.id)
                args.putString("title", it.name)
                args.putString("overview", it.overview)
                args.putString("date",it.first_air_date)
                args.putBoolean("movie",false)
                val dialog = DetailsFragment()
                dialog.arguments = args
                dialog.show(parentFragmentManager,"")
            })
            val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
            rvPlayingNow.addItemDecoration(
                DividerItemDecoration(
                    activity,
                    LinearLayoutManager.VERTICAL
                )
            )
            rvPlayingNow.adapter=adapter
            rvPlayingNow.layoutManager=layoutManager
        })

        moviesViewModel.loadingNowPlaying.observe(this, Observer {
            progressBar.isVisible = it
        })

        moviesViewModel.getNowPlayingMovies()
        switch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                switch.text = getString(R.string.switchSeries)
                moviesViewModel.getNowPlayingTV()
            } else {
                switch.text = getString(R.string.switchPeliculas)
                moviesViewModel.getNowPlayingMovies()
            }
        }

    }
}