package com.dyd.peliculasyseries.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dyd.peliculasyseries.R
import com.dyd.peliculasyseries.data.models.TVModel
import com.dyd.peliculasyseries.data.models.TVOverviewModel
import com.dyd.peliculasyseries.data.network.ApiConstants

class TVShowAdapter(private val tvshow: TVModel,private val onClickListener: OnClickListener) : RecyclerView.Adapter<TVShowAdapter.viewHolder>() {

    class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var TVImage: ImageView = itemView.findViewById(R.id.ivPoster)
        var TVTitle: TextView = itemView.findViewById(R.id.tvTitle)
        var TVExtra: TextView = itemView.findViewById(R.id.tvExtra)
        var TVOverview: TextView = itemView.findViewById(R.id.tvOverview)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row, parent, false)
        return viewHolder(itemView)
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        var title = tvshow.results.get(position).name + " (" +tvshow.results.get(position).vote_average +")"
        holder.TVTitle.text = title
        holder.TVOverview.text = tvshow.results.get(position).overview
        holder.TVExtra.text = tvshow.results.get(position).first_air_date
        Glide.with(holder.itemView)
            .load(ApiConstants.IMG_BASE_URL+tvshow.results.get(position).poster_path)
            .into(holder.TVImage)

        val item = tvshow.results.get(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(item)
        }
    }
    class OnClickListener(val clickListener: (item:TVOverviewModel) -> Unit) {
        fun onClick(item: TVOverviewModel) = clickListener(item)
    }
    override fun getItemCount(): Int {
        return tvshow.results.size
    }
}