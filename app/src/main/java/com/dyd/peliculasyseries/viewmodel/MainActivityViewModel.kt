package com.dyd.peliculasyseries.viewmodel

import MovieModel
import android.app.Activity
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dyd.peliculasyseries.data.Repository
import com.dyd.peliculasyseries.data.models.TVModel
import com.dyd.peliculasyseries.data.models.VideosModel
import com.dyd.peliculasyseries.data.network.RetrofitService
import com.dyd.peliculasyseries.view.MainActivity
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.coroutines.*
import java.lang.Exception
import java.net.InetAddress


class MainActivityViewModel() : ViewModel() {
    val moviesNowPlaying = MutableLiveData<MovieModel>()
    val TVNowPlaying = MutableLiveData<TVModel>()
    val loadingNowPlaying = MutableLiveData<Boolean>()

    val moviesMostPopular = MutableLiveData<MovieModel>()
    val TVMostPopular = MutableLiveData<TVModel>()
    val loadingMostPopular = MutableLiveData<Boolean>()

    val videoTV = MutableLiveData<VideosModel>()
    val videoMovie = MutableLiveData<VideosModel>()

    val retrofitService = RetrofitService.getInstance()
    val repository = Repository(retrofitService)
    var job: Job? = null

    fun getNowPlayingMovies() {
        job = CoroutineScope(Dispatchers.IO).launch {
            loadingNowPlaying.postValue(true)
            if (internetAvailable()) {
                val response = repository.getNowPlayingMovies()
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        moviesNowPlaying.postValue(response.body())
                    }
                }
            }
            loadingNowPlaying.postValue(false)
        }
    }

    fun getMostPopularMovies() {
        job = CoroutineScope(Dispatchers.IO).launch {
            loadingMostPopular.postValue(true)
            if (internetAvailable()) {
                val response = repository.getMostPopularMovies()
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        moviesMostPopular.postValue(response.body())
                    }
                }
            }
            loadingMostPopular.postValue(false)
        }
    }

    fun getNowPlayingTV() {
        job = CoroutineScope(Dispatchers.IO).launch {
            loadingNowPlaying.postValue(true)
            if (internetAvailable()) {
                val response = repository.getNowPlayingTV()
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        TVNowPlaying.postValue(response.body())
                    }
                }
            }
            loadingNowPlaying.postValue(false)
        }
    }

    fun getMostPopularTV() {
        job = CoroutineScope(Dispatchers.IO).launch {
            loadingMostPopular.postValue(true)
            if (internetAvailable()) {
                val response = repository.getMostPopularTV()
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        TVMostPopular.postValue(response.body())
                    }
                }
            }
            loadingMostPopular.postValue(false)
        }
    }

    fun getVideoTV(id: Int) {
        job = CoroutineScope(Dispatchers.IO).launch {
            if (internetAvailable()) {
                val response = repository.getTVVideo(id)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        videoTV.postValue(response.body())
                    }
                }
            }
        }
    }

    fun getVideoMovie(id: Int) {
        job = CoroutineScope(Dispatchers.IO).launch {
            if (internetAvailable()) {
                val response = repository.getMovieVideo(id)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        videoMovie.postValue(response.body())
                    }
                }
            }
        }
    }

    fun internetAvailable(): Boolean {
        return try {
            val ipAddr: InetAddress = InetAddress.getByName("google.com")
            !ipAddr.equals("")
        } catch (e: Exception) {
            false
        }
    }

}